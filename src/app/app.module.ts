import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DataTablesModule } from 'angular-datatables';
import { LoginComponent } from './Pages/Login/login.component';
import { OutletComponent } from './Pages/Outlet/outlet.component';
import { LayoutComponent } from './Pages/Shared/Layout/layout.component';
import { LeftmenuComponent } from './Pages/Shared/LeftMenu/leftmenu.component';
import { ToparComponent } from './Pages/Shared/TopBar/topar.component';
import { FooterComponent } from './Pages/Shared/Footer/footer.component';
import { DashboardComponent } from './Pages/DashBoard/dashboard.component';
import {MasterComponent} from './Pages/Master/master.component'
import { CategoriesComponent } from './Pages/Master/IngredientCategories/categories.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UnitsComponent } from './Pages/Master/IngredientUnits/units.component';
import { IngredientComponent } from './Pages/Master/Ingredients/ingredient.component';
import { ModifiersComponent } from './Pages/Master/Modifiers/modifiers.component';
import { FoodcategoryComponent } from './Pages/Master/FoodMenuCategory/foodcategory.component';
import { FoodmenuComponent } from './Pages/Master/FoodMenu/foodmenu.component';
import { SupplierComponent } from './Pages/Master/Suppliers/supplier.component';
import { CustomerComponent } from './Pages/Master/Customers/customer.component';
import { ExpenseitemComponent } from './Pages/Master/ExpenseItems/expenseitem.component';
import { PaymentmethodComponent } from './Pages/Master/PaymentMethod/paymentmethod.component';
import { TableComponent } from './Pages/Master/Tables/table.component';
import { TestComponent } from './Pages/Test/test.component';
import { AllscreenComponent } from './Pages/All Screens/allscreen.component';
import { PosComponent } from './Pages/All Screens/POS/pos.component';
import { BarComponent } from './Pages/All Screens/Bar/bar.component';
import { KitchenComponent } from './Pages/All Screens/Kitchen/kitchen.component';
import { WaiterComponent } from './Pages/All Screens/Waiter/waiter.component';
import { SaleComponent } from './Pages/Sale/sale.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CustomerService } from './Pages/Master/Customers/Service/customer.service';
import { RegisterComponent } from './Pages/Register/register.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    OutletComponent,
    LayoutComponent,
    LeftmenuComponent,
    ToparComponent,
    FooterComponent,
    DashboardComponent,
    MasterComponent,
    CategoriesComponent,
    UnitsComponent,
    IngredientComponent,
    ModifiersComponent,
    FoodcategoryComponent,
    FoodmenuComponent,
    SupplierComponent,
    CustomerComponent,
    ExpenseitemComponent,
    PaymentmethodComponent,
    TableComponent,
    TestComponent,
    AllscreenComponent,
    PosComponent,
    BarComponent,
    KitchenComponent,
    WaiterComponent,
    SaleComponent,
    RegisterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DataTablesModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule

  ],
  providers: [HttpClientModule,CustomerService  ],
  bootstrap: [AppComponent]

})
export class AppModule { }
