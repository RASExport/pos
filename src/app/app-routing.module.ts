import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllscreenComponent } from './Pages/All Screens/allscreen.component';
import { BarComponent } from './Pages/All Screens/Bar/bar.component';
import { KitchenComponent } from './Pages/All Screens/Kitchen/kitchen.component';
import { PosComponent } from './Pages/All Screens/POS/pos.component';
import { WaiterComponent } from './Pages/All Screens/Waiter/waiter.component';
import { DashboardComponent } from './Pages/DashBoard/dashboard.component';
import { LoginComponent } from './Pages/Login/login.component';
import { CustomerComponent } from './Pages/Master/Customers/customer.component';
import { ExpenseitemComponent } from './Pages/Master/ExpenseItems/expenseitem.component';
import { FoodmenuComponent } from './Pages/Master/FoodMenu/foodmenu.component';
import { FoodcategoryComponent } from './Pages/Master/FoodMenuCategory/foodcategory.component';
import { CategoriesComponent } from './Pages/Master/IngredientCategories/categories.component';
import { IngredientComponent } from './Pages/Master/Ingredients/ingredient.component';
import { UnitsComponent } from './Pages/Master/IngredientUnits/units.component';
import { MasterComponent } from './Pages/Master/master.component';
import { ModifiersComponent } from './Pages/Master/Modifiers/modifiers.component';
import { PaymentmethodComponent } from './Pages/Master/PaymentMethod/paymentmethod.component';
import { SupplierComponent } from './Pages/Master/Suppliers/supplier.component';
import { TableComponent } from './Pages/Master/Tables/table.component';
import { OutletComponent } from './Pages/Outlet/outlet.component';
import { RegisterComponent } from './Pages/Register/register.component';
import { SaleComponent } from './Pages/Sale/sale.component';
import { LayoutComponent } from './Pages/Shared/Layout/layout.component';

const routes: Routes = [
{
  path: '', component: LayoutComponent, children:[
    {path:'Dashboard',component:DashboardComponent},
    {path:'Outlet',component:OutletComponent},
    {path:'Sale',component:SaleComponent},
    {
      path: 'Master', component: MasterComponent, children:[
        { path: 'Categories', component:CategoriesComponent },
        { path: 'Units', component:UnitsComponent },
        { path: 'Ingredient', component:IngredientComponent },
        { path: 'Modifiers', component:ModifiersComponent },
        { path: 'FoodMenuCat', component:FoodcategoryComponent },
        { path: 'FoodMenu', component:FoodmenuComponent},
        { path: 'Supplier', component:SupplierComponent},
        { path: 'Customer', component:CustomerComponent},
        { path: 'Expense', component:ExpenseitemComponent},
        { path: 'PaymentMethod', component:PaymentmethodComponent},
        { path: 'Table', component:TableComponent},
      ]
    },
    {
        path: 'AllScreen', component: AllscreenComponent, children:[
          {path: 'Pos', component:PosComponent},
          {path: 'Bar', component:BarComponent},
          {path: 'Kitchen', component:KitchenComponent},
          {path: 'Waiter', component:WaiterComponent},
        ]
    }

  ]
},
{ path: 'login', component: LoginComponent, pathMatch: 'full' },
{ path: 'register', component: RegisterComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
