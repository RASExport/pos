import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AccountService } from '../Login/Service/account';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
registerForm:FormGroup;
  constructor(private route: ActivatedRoute,
    private accountService: AccountService,
    private router: Router) {
    this.registerForm= new FormGroup({});
   }

  ngOnInit(): void {
    this.registerForm= new FormGroup({
      FirstName: new FormControl(''),
      Password: new FormControl(''),
    })
  }
  onRegisterSubmit() {
    // reset alerts on submit
    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }
    debugger
    this.accountService
      .register(
        this.registerForm.controls.FirstName.value,
        this.registerForm.controls.Password.value
      )
      .pipe(first())
      .subscribe({
        next: () => {
          // get return url from query parameters or default to home page
          const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
          this.router.navigateByUrl(returnUrl);
        },
        // error: error => {
        //     this.alertService.error(error);
        //     this.loading = false;
        // }
      });
  }
}
