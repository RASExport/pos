import { Injectable } from "@angular/core";
import { Subject } from "rxjs";
import { Alert, AlertType } from "../Model/alert";

@Injectable({ providedIn: 'root' })
export class AlertService {
  private defaultId = 'default-alert';
  private subject = new Subject<Alert>();
  error(message: string, options?: any) {
    this.alert(new Alert({ ...options, type:
      AlertType.Error, message }));
}
// main alert method
alert(alert: Alert) {
  alert.id = alert.id || this.defaultId;
  this.subject.next(alert);
}
}
