import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { environment } from 'src/environments/environment';
import { User } from '../Model/user';

@Injectable({ providedIn: 'root' })
export class AccountService {
  //private userSubject: BehaviorSubject<User>;
  //public user: Observable<User>;

  constructor(private router: Router, private http: HttpClient) {
    //this.userSubject = new BehaviorSubject<User>(JSON.parse('user'));
    //this.user = this.userSubject.asObservable();
  }

  // public get userValue(): User {
  //   return this.userSubject.value;
  // }

  login(Username: any, Password: any) {
    return this.http.post<User>(`${environment.apiUrl}/Account/Login`, { Username, Password })
      .pipe(
        map((user) => {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('user', JSON.stringify(user));
          //this.userSubject.next(user);
          return user;
        })
      );
  }

  register(FirstName: any, Password: any) {
    return this.http.post(`${environment.apiUrl}/Account/Register`, {FirstName,Password});
  }
}
