import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { first } from 'rxjs/operators';
import { Alert, AlertType } from './Model/alert';
import { AccountService } from './Service/account';
import { AlertService } from './Service/alert';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  constructor(
    private route: ActivatedRoute,
    private accountService: AccountService,
    private router: Router,
    private alertService: AlertService
  ) {
    this.loginForm = new FormGroup({});
  }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      Username: new FormControl(''),
      Password: new FormControl(''),
    });
  }

  onLoginSubmit() {
    // reset alerts on submit
    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }
    this.accountService
      .login(
        this.loginForm.controls.Username.value,
        this.loginForm.controls.Password.value
      )
      .pipe(first())
      .subscribe({
        next: () => {
          // get return url from query parameters or default to home page
          const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
          this.router.navigateByUrl(returnUrl);
        },
        error: error => {
            this.alertService.error(error);
            // this.loading = false;
        }
      });
  }
}



