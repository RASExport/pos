import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { from } from 'rxjs';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
tableForm:  FormGroup;
isShow=false;
  constructor() {
    this.tableForm= new FormGroup({});
   }
addNewTable(){
  this.isShow=!this.isShow;
}
  ngOnInit(): void {
    this.tableForm= new FormGroup({
      'tableForm' : new FormControl(''),
      'SeatCapacity' : new FormControl(''),
      'Position' : new FormControl(''),
      'Description' : new FormControl(''),
      'Outlet' : new FormControl(''),
    })
  }
  onTableSubmit(){
    console.log(this.tableForm);
  }
}
