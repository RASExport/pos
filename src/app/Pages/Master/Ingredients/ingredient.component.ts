import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.css']
})
export class IngredientComponent implements OnInit {
  ingredientForm:FormGroup;
  isShow = false;
  constructor() {
    this.ingredientForm = new FormGroup({});
   }
 addNewIngredient(){
   this.isShow=!this.isShow;
 }
  ngOnInit(): void {
    this.ingredientForm= new FormGroup({
     'name': new FormControl(''),
     'category': new FormControl(''),
      'unit': new FormControl(''),
      'purchaseprice': new FormControl(''),
      'alertqty': new FormControl(''),
      'code': new FormControl(''),
    });
  }
   onIgredientSubmit(){
     console.log(this.ingredientForm);
   }
}
