import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-modifiers',
  templateUrl: './modifiers.component.html',
  styleUrls: ['./modifiers.component.css']
})
export class ModifiersComponent implements OnInit {
  modifierForm:FormGroup;
  isShow=false;
  constructor() {
    this.modifierForm=new FormGroup({});
   }
addNewModifier(){
  this.isShow=!this.isShow;
}
  ngOnInit(): void {
    this.modifierForm=new FormGroup({
      'name':new FormControl(''),
      'price': new FormControl(''),
      'ingredientConsumption' : new FormControl(''),
      'description': new FormControl(''),

    });
  }
onModifierSubmit(){
  console.log(this.modifierForm);
}
}
