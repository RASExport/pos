import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-units',
  templateUrl: './units.component.html',
  styleUrls: ['./units.component.css']
})
export class UnitsComponent implements OnInit {
  ingredientUnitForm:FormGroup;
 isShow=false;
  constructor() {
    this.ingredientUnitForm = new FormGroup({});
  }
addNewUnits(){
  this.isShow=!this.isShow;
}
  ngOnInit(): void {
    this.ingredientUnitForm = new FormGroup({
        'categoryname': new FormControl(''),
        'description': new FormControl(''),
    });
  }
  onIngredientUnitSubmit(){
    console.log(this.ingredientUnitForm);
  }
}
