import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { ApiService } from 'src/app/Services/API/api.service';
import { AlertService } from '../../Login/Service/alert';
import { requestCustomer } from './Model/customer';
import { CustomerService } from './Service/customer.service';
@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.css']
})
export class CustomerComponent implements OnInit {
  customerForm: FormGroup;
  requestCustomer= new requestCustomer();
  isShow=false;
  massage = null;
  constructor(

    private api:ApiService,
    private route: ActivatedRoute,
    private router: Router,
    private alertService: AlertService
    ) {

     this.customerForm= new FormGroup({});
     this.requestCustomer= new requestCustomer();
    }
addNewCustomer(){
  this.isShow=!this.isShow;

}
InitializeForm():any{
  this.customerForm= new FormGroup({
    CustomerName : new FormControl(''),
    Phone : new FormControl(''),
    Email : new FormControl(''),
    DateOfBirth : new FormControl(''),
    DateOfAnniversary : new FormControl(''),
    Address : new FormControl(''),
  })
}
  ngOnInit(): void {
    this.InitializeForm();
  }
  onCustomerSubmit(){
     //this.addCustomer(customer);
  }
  SaveCustomer(){

    this.requestCustomer.Name=this.customerForm.controls.CustomerName.value;
  this.requestCustomer.Address=this.customerForm.controls.Address.value;
  this.requestCustomer.Dob=this.customerForm.controls.DateOfBirth.value;
  this.requestCustomer.Email=this.customerForm.controls.Email.value;
  this.requestCustomer.Phone=this.customerForm.controls.Phone.value;
  this.
      api.addCustomer(this.requestCustomer)
      .pipe(first())
      .subscribe({
        next: () => {
          // get return url from query parameters or default to home page
          const returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
          this.router.navigateByUrl(returnUrl);
        },
        error: error => {
            this.alertService.error(error);
            // this.loading = false;
        }
      });


 }
}
