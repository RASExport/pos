import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-supplier',
  templateUrl: './supplier.component.html',
  styleUrls: ['./supplier.component.css']
})
export class SupplierComponent implements OnInit {
  supplierForm: FormGroup;
isShow=false;
  constructor() {
    this.supplierForm= new FormGroup({});
  }
addNewSupplier(){
  this.isShow=!this.isShow;
}
  ngOnInit(): void {
    this.supplierForm = new FormGroup({
      'SupplierName' : new FormControl(''),
      'ContactPerson' : new FormControl(''),
      'Phone' : new FormControl(''),
      'Email' : new FormControl(''),
      'Address' : new FormControl(''),
      'Description' : new FormControl('')
    })
  }
  onSupplierSubmit(){
    console.log(this.supplierForm);
  }
}
