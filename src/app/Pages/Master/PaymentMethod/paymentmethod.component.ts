import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-paymentmethod',
  templateUrl: './paymentmethod.component.html',
  styleUrls: ['./paymentmethod.component.css']
})
export class PaymentmethodComponent implements OnInit {
  paymentMethodForm: FormGroup;
isShow=false;
  constructor() {
    this.paymentMethodForm= new FormGroup({})
   }
addNewPaymentMethod(){
  this.isShow=!this.isShow;
}
  ngOnInit(): void {
    this.paymentMethodForm = new FormGroup({
      'PaymentMethodName' : new FormControl(''),
      'Description' : new FormControl('')
    })
  }
onPaymentMethodSubmit(){
  console.log(this.paymentMethodForm);
}
}
