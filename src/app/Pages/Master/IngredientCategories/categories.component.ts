import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl, ReactiveFormsModule  } from '@angular/forms';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {
  isShow = false;
  ingredientCategoryForm:FormGroup;
  addNewCategories() {
    this.isShow = !this.isShow;
  }
  constructor() {
    this.ingredientCategoryForm= new FormGroup({});
   }

  ngOnInit(): void {
   this.ingredientCategoryForm=new FormGroup({
      'categoryname': new FormControl(''),
      'description': new FormControl(''),
   });
  }
   onIngredientCategory(){
     console.log(this.ingredientCategoryForm)
   }
}
