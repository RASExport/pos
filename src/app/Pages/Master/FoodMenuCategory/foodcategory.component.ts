import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-foodcategory',
  templateUrl: './foodcategory.component.html',
  styleUrls: ['./foodcategory.component.css']
})
export class FoodcategoryComponent implements OnInit {
  foodCategoryForm: FormGroup;
isShow=false;
  constructor() {
    this.foodCategoryForm = new FormGroup({});
  }
addNewFoodCategory(){
  this.isShow=!this.isShow;
}
  ngOnInit(): void {
    this.foodCategoryForm = new FormGroup({
       'categoryname': new FormControl(''),
       'description': new FormControl(''),
    });
  }
 onFoodCategorySubmit(){
   console.log(this.foodCategoryForm);
 }
}
