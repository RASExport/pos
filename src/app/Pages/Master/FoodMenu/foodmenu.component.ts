import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-foodmenu',
  templateUrl: './foodmenu.component.html',
  styleUrls: ['./foodmenu.component.css'],
})
export class FoodmenuComponent implements OnInit {
  foodMenuForm:FormGroup;
  isShow = false;
  isShowFoodMenu = false;
  isShowUploadMenu=false;
  addFoodMenu = true;
  uploadFoodMenuButton = false;
  showTable = true;
  constructor() {
    this.foodMenuForm= new FormGroup({});
  }
  addNewFoodMenu() {
    this.isShow = !this.isShow;
    this.showTable = !this.showTable;
  }
  uploadFoodMenu() {
    this.isShow = !this.isShow;
    this.isShowFoodMenu = !this.isShowFoodMenu;
    this.uploadFoodMenuButton = true;
    this.addFoodMenu = false;
  }
  uploadRecipt(){
    this.isShow = !this.isShow;
    this.isShowUploadMenu = !this.isShowUploadMenu;
    this.addFoodMenu = false;
  }
  ngOnInit(): void {
    this.foodMenuForm= new FormGroup({
      'Name' : new FormControl(''),
      'Code' : new FormControl(''),
      'Category' : new FormControl(''),
      'IngredientConsumption' : new FormControl(''),
      'SalePrice' : new FormControl(''),
      'Description' : new FormControl(''),
      'Photo' : new FormControl(''),
      'VegItem' : new FormControl(''),
      'IsItBeverage' : new FormControl(''),
      'IsItBarItem' : new FormControl(''),
      'Vat' : new FormControl(''),
    })
  }
  onFoodMenuSubmit(){
    console.log(this.foodMenuForm);
  }
}
