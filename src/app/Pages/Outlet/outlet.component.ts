import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-outlet',
  templateUrl: './outlet.component.html',
  styleUrls: ['./outlet.component.css'],
})
export class OutletComponent implements OnInit {
  isShow = false;
  outletForm: FormGroup;
  addNewOutlets() {
    this.isShow = !this.isShow;
  }
  constructor() {
    this.outletForm = new FormGroup({});
  }

  ngOnInit(): void {
    this.outletForm = new FormGroup({
      'outletcode': new FormControl(''),
      'outletname': new FormControl(''),
      'phone':new FormControl(''),
      'email': new FormControl(''),
      'address': new FormControl(''),
      'status':new FormControl('Active'),
      'waiter': new FormControl('zak'),
      'kitchen':new FormControl(''),
    });
  }
  onOutletsubmit(){
     console.log(this.outletForm)
  }
}
