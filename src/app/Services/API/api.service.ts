import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient, private router: Router) {}
  addCustomer(customer: any) {
    return this.http.post(`${environment.apiUrl}/Customer/AddCustomer/`,
      customer
    );
  }
}
